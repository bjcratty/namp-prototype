/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, Component, useCallback, useMemo, useRef } from 'react';
import { Text, StyleSheet, Animated, View, Image, Dimensions, ScrollView, TouchableOpacity, SafeAreaView, Button, TouchableHighlight, Platform } from 'react-native';
import styles from "../styles/pdfreader.style";
import { SafeAreaProvider } from 'react-native-safe-area-context';
import bottomSheet from '../styles/bottomSheet.style';
// import PDFs from '@assets/pdfs'//https://willowtreeapps.com/ideas/react-native-tips-and-tricks-2-0-managing-static-assets-with-absolute-paths
import PDFs from "../assets/pdfs";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
// import Animated, { Easing } from 'react-native-reanimated';
import BottomSheet from '@gorhom/bottom-sheet';
import ScrollBottomSheet from 'react-native-scroll-bottom-sheet';
import * as utils from '../utils/sanitizers';
import 'react-native-gesture-handler';
import * as theme from '../theme';
// import { FlatList } from 'react-native-gesture-handler';
import Pdf from 'react-native-pdf';
import tableOfContents from "../static/tableOfContents";
import * as global from "../config/global";

//Constants to be used accross the component
const { width, height } = Dimensions.get('window');
const WIN_WIDTH = Dimensions.get('window').width;
const WIN_HEIGHT = Dimensions.get('window').height;
const windowHeight = Dimensions.get('window').height;
const scrollX = new Animated.Value(0);

//load fonts 
FontAwesome.loadFont();
MaterialIcons.loadFont();

const PDFReader = props => {

// const source = (Platform.OS == 'ios') ? require('../assets/Chapter01.pdf') : {uri:'bundle-assets://test.pdf'};
// const source = {uri:"/Users/brandonsmac/SysDevMobileApps/FrameworkPrototypes/namp-prototype/src/assets/namp_docs.pdf"}
const source = PDFs.NAMP_Doc
  // const source = {uri:'http://samples.leanpub.com/thereactnativebook-sample.pdf',cache:true};
  // let source = require('../assets/Chapter01.pdf'); // ios only
  // let source = require('./example/ios/assets/test.pdf'); // ios only
  // let source = require('../../../example/ios/assets/test.pdf'); // ios only
  // const source = {uri:'bundle-assets://test.pdf'};

  // const source = {uri:'file:///test.pdf'};
  //const source = {uri:"data:application/pdf;base64,JVBERi0xLjcKJc..."};

  const [data, setData] = useState({
    page: 1,
    scale: 1,
    numberOfPages: 0,
    horizontal: false,
    width: WIN_WIDTH,
    tableContents: [],
  });
  const [pdf, setPdf] = useState(null);

  //hooks for the bottom sheet
  // ref, this below is typescript syntax
  // const bottomSheetRef = useRef<BottomSheet>(null);

  // variables
  const snapPoints = useMemo(() => ['25%', '50%'], []);

  // callbacks
  const handleSheetChanges = useCallback((index: number) => {
    console.log('handleSheetChanges', index);
  }, []);

  const [counter, setCounter] = useState(0);
  // const openBottomSheet = () => {
  //   // comment out setCounter and the bottom sheet opens as expected
  //   setCounter((prev) => prev + 1);
  //   bottomSheetRef.current?.snapTo(1);
  // };

  //this is the rendered item, this is what is generated per element in the tableOfContents
  const renderItem = ({ item }) => (
    <View style={bottomSheet.item}>
      <TouchableHighlight onPress={()=> pdf.setPage(Number(item.pageIdx))}>
        <Text style={{color: '#F3F4F9'}}>{`${item.title}`}</Text>
      </TouchableHighlight>
    </View>
  );


  //get initial height of bottom sheet to start off at
  const lowPoint = (Platform.OS == 'ios') ? 100 : 50;
  const snapPointsArray = (global.NavigationType == 'Tab') ? [128, '50%', windowHeight - (200 - lowPoint)] : [128, '50%', windowHeight - (100 - lowPoint)];

  return (

    <SafeAreaView style={styles.fileContainer}>
      <View style={{ flex: 1, width: data.width }}>
        {/* <Button onPress={openBottomSheet} title="Open bottom sheet" /> */}
        <Pdf
          ref={(pdf) => {
            setPdf(pdf);
          }}
          source={source}
          scale={data.scale}
          horizontal={data.horizontal}
          onLoadComplete={(
            numberOfPages,
            filePath,
            { width, height },
            tableContents,
          ) => {
            setData({
              ...data,
              numberOfPages: numberOfPages,
              tableContents: utils.buildBookmarks(tableContents),
            });
            console.log(`total page count: ${numberOfPages}`);
            // console.log(data.tableContents);
          }}
          onPageChanged={(page, numberOfPages) => {
            setData({
              ...data,
              page: page
            });
            console.log(`current page: ${page}`);
          }}
          onError={(error) => {
            console.log("error", error);
          }}
          style={{ flex: 1 }}
        />
        {/* <ScrollBottomSheet<string> // If you are using TS, that'll infer the renderItem `item` type
        componentType="FlatList"
        snapPoints={[128, '50%', windowHeight - 200]}
        initialSnapIndex={2}
        renderHandle={() => (
          <View style={styles.header}>
            <View style={styles.panelHandle} />
          </View>
        )}
        data={Array.from({ length: 200 }).map((_, i) => String(i))}
        keyExtractor={i => i}
        renderItem={({ item }) => (
          <View style={styles.item}>
            <Text>{`Item ${item}`}</Text>
          </View>
        )}
        contentContainerStyle={styles.contentContainerStyle}
      /> */}
        <ScrollBottomSheet // If you are using TS, that'll infer the renderItem `item` type
          componentType="FlatList"
          // snapPoints={snapPointsArray}
          snapPoints={['7%', '50%', '80%']}
          // snapPoints={[128, '50%', windowHeight - 200]}//change to about 50 if android
          initialSnapIndex={2}
          //this is the bottom sheet container itself 
          renderHandle={() => (
            <View style={bottomSheet.header}>
              <View style={bottomSheet.panelHandle} />
            </View>
          )}
          data={data.tableContents}
          keyExtractor={(item, index) => String(index)}
          renderItem={renderItem}
          contentContainerStyle={bottomSheet.contentContainerStyle}
        />
      </View>
    </SafeAreaView>
  );
  // }
}


PDFReader.navigationOptions = ({ navigation }) => {
  return {
    header: () =>
      <View style={[styles.flex, styles.row, styles.header]}>
        <TouchableOpacity
          style={styles.back}
          onPress={() => navigation.goBack()}>
          <FontAwesome
            name="chevron-left"
            color={theme.colors.blue}
            size={theme.sizes.font * 1}
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <MaterialIcons
            name="more-horiz"
            color={theme.colors.blue}
            size={theme.sizes.font * 1.5}
          />
        </TouchableOpacity>
      </View>,
    headerTransparent: true,
  };
};



export default PDFReader;
