import React, { Component } from 'react';
import { Platform, View, ScrollView, Text, StatusBar, SafeAreaView, Button, Image, TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from '../styles/SliderEntry.style';
import SliderEntry from '../components/SliderEntry';
import styles, { colors } from '../styles/index.style';
import { ENTRIES3 } from '../static/entries';
import { scrollInterpolators, animatedStyles } from '../utils/animations';
import QuizEntry from "../components/QuizEntry";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Octicons from 'react-native-vector-icons/Octicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import * as theme from '../theme';
import { SearchBar } from 'react-native-elements';

Octicons.loadFont();
FontAwesome.loadFont();
MaterialIcons.loadFont();


const IS_ANDROID = Platform.OS === 'android';
const SLIDER_1_FIRST_ITEM = 1;

export default class CarouselPage extends Component {
    static navigationOptions = { headerShown: false }

    constructor (props) {
        super(props);
        this.state = {
            slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
            search: ''
        };
    }
    
    updateSearch = (search) => {
        this.setState({search});
    }


    _renderItem ({item, index}) {
        return <SliderEntry data={item} even={(index + 1) % 2 === 0} navigation={this.props.navigation} />;
    }


    _renderQuiz ({item, index}) {
        return <QuizEntry data={item} even={(index + 1) % 2 === 0} navigation={this.props.navigation} />;
    }

    _renderItemWithParallax ({item, index}, parallaxProps) {
        return (
            <SliderEntry
              data={item}
              even={(index + 1) % 2 === 0}
              parallax={true}
              parallaxProps={parallaxProps}
              navigation={this.props.navigation}
            />
        );
    }

    _renderLightItem ({item, index}) {
        return <SliderEntry data={item} even={false} />;
    }

    _renderDarkItem ({item, index}) {
        return <SliderEntry data={item} even={true} />;
    }

    mainExample (number, title, subtitle) {
        const { slider1ActiveSlide } = this.state;

        return (
            <View style={styles.exampleContainer}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.subtitle}>{subtitle}</Text>
                <Carousel
                  ref={c => this._slider1Ref = c}
                  data={ENTRIES3}
                  renderItem={this._renderItem.bind(this)} 
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidth}
                  hasParallaxImages={true}
                  firstItem={SLIDER_1_FIRST_ITEM}
                  inactiveSlideScale={0.94}
                  inactiveSlideOpacity={0.7}
                  // inactiveSlideShift={20}
                  containerCustomStyle={styles.slider}
                  contentContainerCustomStyle={styles.sliderContentContainer}
                  loop={true}
                  loopClonesPerSide={2}
                  autoplay={false}
                  autoplayDelay={500}
                  autoplayInterval={3000}
                  onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                />
                <Pagination
                  dotsLength={ENTRIES3.length}
                  activeDotIndex={slider1ActiveSlide}
                  containerStyle={styles.paginationContainer}
                  dotColor={'rgba(255, 255, 255, 0.92)'}
                  dotStyle={styles.paginationDot}
                  inactiveDotColor={colors.black}
                  inactiveDotOpacity={0.4}
                  inactiveDotScale={0.6}
                  carouselRef={this._slider1Ref}
                  tappableDots={!!this._slider1Ref}
                />
            </View>
        );
    }

    mainExample2 (number, title, subtitle) {
        const { slider1ActiveSlide } = this.state;

        return (
            <View style={styles.exampleContainer}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.subtitle}>{subtitle}</Text>
                <Carousel
                  ref={c => this._slider1Ref = c}
                  data={ENTRIES3}
                  renderItem={this._renderQuiz.bind(this)} 
                  sliderWidth={sliderWidth}
                  itemWidth={itemWidth}
                  hasParallaxImages={true}
                  firstItem={SLIDER_1_FIRST_ITEM}
                  inactiveSlideScale={0.94}
                  inactiveSlideOpacity={0.7}
                  // inactiveSlideShift={20}
                  containerCustomStyle={styles.slider}
                  contentContainerCustomStyle={styles.sliderContentContainer}
                  loop={true}
                  loopClonesPerSide={2}
                  autoplay={true}
                  autoplayDelay={500}
                  autoplayInterval={3000}
                  onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                />
                <Pagination
                  dotsLength={ENTRIES3.length}
                  activeDotIndex={slider1ActiveSlide}
                  containerStyle={styles.paginationContainer}
                  dotColor={'rgba(255, 255, 255, 0.92)'}
                  dotStyle={styles.paginationDot}
                  inactiveDotColor={colors.black}
                  inactiveDotOpacity={0.4}
                  inactiveDotScale={0.6}
                  carouselRef={this._slider1Ref}
                  tappableDots={!!this._slider1Ref}
                />
            </View>
        );
    }
    // momentumExample (number, title) {
    //     return (
    //         <View style={styles.exampleContainer}>
    //             <Text style={styles.title}>{`Quizzes`}</Text>
    //             <Text style={styles.subtitle}>{title}</Text>
    //             <Carousel
    //               data={ENTRIES2}
    //               renderItem={this._renderItem}
    //               sliderWidth={sliderWidth}
    //               itemWidth={itemWidth}
    //               inactiveSlideScale={0.95}
    //               inactiveSlideOpacity={1}
    //               enableMomentum={true}
    //               activeSlideAlignment={'start'}
    //               containerCustomStyle={styles.slider}
    //               contentContainerCustomStyle={styles.sliderContentContainer}
    //               activeAnimationType={'spring'}
    //               activeAnimationOptions={{
    //                   friction: 4,
    //                   tension: 40
    //               }}
    //             />
    //         </View>
    //     );
    // }

    // layoutExample (number, title, type) {
    //     const isTinder = type === 'tinder';
    //     return (
    //         <View style={[styles.exampleContainer, isTinder ? styles.exampleContainerDark : styles.exampleContainerLight]}>
    //             <Text style={[styles.title, isTinder ? {} : styles.titleDark]}>{`Example ${number}`}</Text>
    //             <Text style={[styles.subtitle, isTinder ? {} : styles.titleDark]}>{title}</Text>
    //             <Carousel
    //               data={isTinder ? ENTRIES2 : ENTRIES1}
    //               renderItem={isTinder ? this._renderLightItem : this._renderItem}
    //               sliderWidth={sliderWidth}
    //               itemWidth={itemWidth}
    //               containerCustomStyle={styles.slider}
    //               contentContainerCustomStyle={styles.sliderContentContainer}
    //               layout={type}
    //               loop={true}
    //             />
    //         </View>
    //     );
    // }

    // customExample (number, title, refNumber, renderItemFunc) {
    //     const isEven = refNumber % 2 === 0;

    //     // Do not render examples on Android; because of the zIndex bug, they won't work as is
    //     return !IS_ANDROID ? (
    //         <View style={[styles.exampleContainer, isEven ? styles.exampleContainerDark : styles.exampleContainerLight]}>
    //             <Text style={[styles.title, isEven ? {} : styles.titleDark]}>{`Example ${number}`}</Text>
    //             <Text style={[styles.subtitle, isEven ? {} : styles.titleDark]}>{title}</Text>
    //             <Carousel
    //               data={isEven ? ENTRIES2 : ENTRIES1}
    //               renderItem={renderItemFunc}
    //               sliderWidth={sliderWidth}
    //               itemWidth={itemWidth}
    //               containerCustomStyle={styles.slider}
    //               contentContainerCustomStyle={styles.sliderContentContainer}
    //               scrollInterpolator={scrollInterpolators[`scrollInterpolator${refNumber}`]}
    //               slideInterpolatedStyle={animatedStyles[`animatedStyles${refNumber}`]}
    //               useScrollView={true}
    //             />
    //         </View>
    //     ) : false;
    // }

    get gradient () {
        return (
            <LinearGradient
              colors={[colors.background4, colors.background3]}
              startPoint={{ x: 1, y: 0 }}
              endPoint={{ x: 0, y: 1 }}
              style={styles.gradient}
            />
        );
    }

    render () {
        const { search } = this.state;
        const example1 = this.mainExample(1, 'Chapters', 'Select a section of the NAMP you would like to read');
        const example2 = this.mainExample2(2, 'Quizzes', 'Select corresponding quizzes');
        // const example2 = this.momentumExample(2, 'Momentum | Left-aligned | Active animation');
        // const example3 = this.layoutExample(3, '"Stack of cards" layout | Loop', 'stack');
        // const example4 = this.layoutExample(4, '"Tinder-like" layout | Loop', 'tinder');
        // const example5 = this.customExample(5, 'Custom animation 1', 1, this._renderItem);
        // const example6 = this.customExample(6, 'Custom animation 2', 2, this._renderLightItem);
        // const example7 = this.customExample(7, 'Custom animation 3', 3, this._renderDarkItem);
        // const example8 = this.customExample(8, 'Custom animation 4', 4, this._renderLightItem);

        return (
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.container}>
                    <StatusBar
                      translucent={true}
                      backgroundColor={'rgba(0, 0, 0, 0.3)'}
                      barStyle={'light-content'}
                    />
                    { this.gradient }
                    <ScrollView
                      style={styles.scrollview}
                      scrollEventThrottle={200}
                      directionalLockEnabled={true}
                    >
            <SearchBar
                    placeholder="Type Here..."
                    onChangeText={this.updateSearch}
                    value={search}
                    round={true}
                    showCancel={true}
                />
                        { example1 }
                        { example2 }
                        {/* { example2 } */}
                        {/* { example3 }
                        { example4 }
                        { example5 }
                        { example6 }
                        { example7 }
                        { example8 } */}
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}
