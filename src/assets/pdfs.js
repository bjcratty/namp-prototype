const pdfs = {
	test: require("./pdfs/test.pdf"),
	Chapter01: require("./pdfs/Chapter01.pdf"),
	NAMP_Doc: require("./pdfs/namp_docs.pdf"),
};

export default pdfs