const colors = {
  black: '#000',
  white: '#FFF',
  gray: '#DCE0E9',
  caption: '#BCCCD4',
  active: '#007BFA',
  navy_blue: '#1f2353',
  blue: '#0e4d9b',
  light_blue: '#0575e6'
};

const sizes = {
  base: 16,
  font: 14,
  padding: 36,
  margin: 36,
  title: 24,
  radius: 12,
};

export {
  colors,
  sizes,
};