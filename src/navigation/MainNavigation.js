import React from "react";
import { createStackNavigator } from "react-navigation-stack";
import * as global from "../config/global";
import PDFReader from "../screens/PDFReader";
import QuizNavigation from "../navigation/QuizNavigation";
import Quiz from "../screens/Quiz";
import CarouselPage from "../screens/CarouselPage";
import SurveyCompletedScreen from "../screens/SurveyCompletedScreen";
import { SafeAreaProvider } from 'react-native-safe-area-context';

// You can import Ionicons from @expo/vector-icons if you use Expo or
// react-native-vector-icons/Ionicons otherwise.
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

// To see what the icons are go to https://ionicons.com/v4/cheatsheet.html
Ionicons.loadFont();

export const TabNav = createBottomTabNavigator(
  {
    CarouselPage,
    PDFReader,
    QuizNavigation,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'CarouselPage') {
          iconName = focused
            ? 'home-outline'
            : 'ios-information-circle-outline';
          // Sometimes we want to add badges to some icons.
          // You can check the implementation below.
          // IconComponent = HomeIconWithBadge;
        } else if (routeName === 'PDFReader') {
          iconName = 'journal';
        }else if (routeName === 'QuizNavigation') {
          iconName = 'school'; //maybe even try `ribbon`
        }

        // You can return any component that you like here!
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#003e86',
      inactiveTintColor: 'gray',
    },
  }
);


export const StackNav = createStackNavigator(
  {
    CarouselPage,
    PDFReader,
    Quiz,
    SurveyCompletedScreen
  },
  {
   initialRouteName: "CarouselPage",
  }
);

//create a 
// global.NavigationType = "Tab";
// if(global.NavigationType == "Tab"){
//   export default TabNav;
// }
// else{
//   export default StackNav;
// }
