import React from "react";
import { createStackNavigator } from "react-navigation-stack";
import Quiz from "../screens/Quiz";
import QuizCarousel from "../screens/QuizCarousel"
import SurveyCompletedScreen from "../screens/SurveyCompletedScreen";
import { SafeAreaProvider } from 'react-native-safe-area-context';

export default createStackNavigator(
  {
    QuizCarousel,
    Quiz,
    Results: SurveyCompletedScreen
  },
  {
   initialRouteName: "QuizCarousel",
  }

);
