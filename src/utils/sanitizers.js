import React from 'react'

function flattenTree(tree) {
  if (!tree) {
    return [];
  }

  if (tree.children) {
    const result = tree.children.reduce(
      (prev, current) => prev.concat(flattenTree(current)),
      [tree]
    );
    return result;
  } else {
    return [tree];
  }
}

export const buildBookmarks = (bookTree) => {
  var obj = [];
  console.log("booktree", bookTree);
  for (var i = 0; i < bookTree.length; i++) {
    if (bookTree[i].children && bookTree[i].children >= 1) {
      Object.assign(bookTree[i], { hasChildren: true });
    }
    else {
      Object.assign(bookTree[i], { hasChildren: false });
    }

    //add unique id to all the objects
    // Object.assign(bookTree[i], {uniq_id: i.toString()});
    if(bookTree[i].title != "HOME"){
      var list = flattenTree(bookTree[i]);
      obj.push(list);
    }
  }

  //the following will print out all the titles as a list
  //   const ids = plain.map((value) => value.title);
  var merged = [].concat.apply([], obj);
  // console.log(merged);

  //pop off the Home method aka the 1st, which is where home is stored
console.log("final result", merged);
  // merged.shift();
  return merged;

}