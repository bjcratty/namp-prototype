import 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import React from 'react';
import { createAppContainer } from 'react-navigation';
import * as Nav from './navigation/MainNavigation';
import * as global from "./config/global";

const NavType = (global.NavigationType == 'Tab') ? Nav.TabNav : Nav.StackNav;

export default createAppContainer(NavType);