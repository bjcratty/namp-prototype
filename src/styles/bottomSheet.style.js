import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    // container: {
    //     flex: 1,
    //     padding: 24,
    //     backgroundColor: 'grey',
    //   },
    //   contentContainer: {
    //     flex: 1,
    //     alignItems: 'center',
    //   },

    container: {
      // width: '80%',
      flex: 1
    },
    contentContainerStyle: {
      padding: 16,
      // backgroundColor: '#F3F4F9',
      backgroundColor: '#313131'
      // width: '95%',
      // borderTopLeftRadius: 20,
      // borderTopRightRadius: 20
    },
    header: {
      alignItems: 'center',
      backgroundColor: '#414141',
      paddingVertical: 20,
      borderTopLeftRadius: 20,
      borderTopRightRadius: 20
    },
    panelHandle: {
      width: 40,
      height: 4,
      // backgroundColor: 'rgba(0,0,0,0.3)',
      backgroundColor: '#F3F4F9',
      opacity: .3,
      borderRadius: 4
    },
    item: {
      padding: 20,
      justifyContent: 'center',
      // backgroundColor: 'white',
      backgroundColor: '#525252',
      color: '#F3F4F9',
      // opacity: .4,
      alignItems: 'center',
      marginVertical: 10,
      borderRadius: 15,
    },
  });