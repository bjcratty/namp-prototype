**[React Native Notes]{.ul}**

To get setup I highly recommend just follow the instructions at
<https://reactnative.dev/docs/getting-started> instructions. This should
be a relatively smooth process.

Below is information on React Native environment setup, React Native
libraries, how to's, interesting errors that I encountered, etc.

Tools Needed:

-   Android studio
    -   <https://developer.android.com/studio>

-   Cocoapods(<https://guides.cocoapods.org/using/getting-started.html>)
    -   sudo gem install cocoapods

-   Brew(https://brew.sh/
    -   /bin/bash -c \"\$(curl -fsSL
        <https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh>)\"

-   Node
    -   Brew install node, or go to the site and download which is what
        I do
        -   <https://nodejs.org/en/download/>

-   VS Code
    -   <https://code.visualstudio.com/download>
    -   \*\*optional \*\*how to install the \`code\` command for
        productivity purposes, this allows you to open files from
        terminal in VS Code, for example code . will open up whatever
        your current location is in VS Code
        <https://stackoverflow.com/questions/29955500/code-not-working-in-command-line-for-visual-studio-code-on-osx-mac>

-   React Native CLI
    -   To install it only within your project, just navigate to the
        project root directory (/namp-prototype/) and run npm install
        react-native-cli
    -   If you want to install it so that is on your computer and will
        work regardless of the directory you are on run npm install -g
        react-native-cli, this will download it globally on your
        computer

Clone the repo to your computer into a new directory

1.  So create a folder to store the code in, this is whatever you
    prefer. No wrong answer with this just stay organized

2.  So make this folder at the /Users/\<user account name\>/ location,
    ie for me it is /Users/brandonsmac/SysDevMobileApps/

3.  Now after making this folder run git clone
    <https://gitlab.com/bjcratty/namp-prototype/>

4.  Now you have the code downloaded to your computer, just cd
    namp-prototype to get into the project folder

Open the namp prototype folder in VS Code, when opening up XCode or
Android Studio use the platform specific folder (ie.
/namp-prototype/ios/ or /namp-prototype/android/

\*\*\*if you get the below error, you will need to open up the project
folder in Xcode and spin up a simulator that it knows to link to.

![Graphical user interface, text Description automatically
generated](screenshots/image1.png)

After cd ios

Pod install

Cd ..

npm run build:ios-a

Open up xcode\
This will open up finder(file explorer), navigate to the project folder,
and open it so you can find the ios folder, then double click the ios
folder. This will open up the the ios project in Xcode.

Then selected whatever simulator you would like at the top left of the
screen, then click run

This was simply just to get the simulator linked to the project so it
knows where to build. If you ever run into the above error when running
react-native run-ios, just repeat these steps

Now when ever you want to make changes to the app itself you will need
to make the changes, save the files, then re-run the npm run build:ios-a
&& react-native run-ios commands.\
\
TA DA!!

Follow the getting started tutorial at
<https://reactnative.dev/docs/getting-started>

Android\
install android studio

**Install the Android SDK**

Android Studio installs the latest Android SDK by default. Building a
React Native app with native code, however, requires the Android 10
(Q) SDK in particular. Additional Android SDKs can be installed through
the SDK Manager in Android Studio.

To do that, open Android Studio, click on \"Configure\" button and
select \"SDK Manager\".

![Android Studio Welcome](screenshots/image2.png)

The SDK Manager can also be found within the Android Studio
\"Preferences\" dialog, under **Appearance & Behavior** → **System
Settings** → **Android SDK**.

Select the \"SDK Platforms\" tab from within the SDK Manager, then check
the box next to \"Show Package Details\" in the bottom right corner.
Look for and expand the Android 10 (Q) entry, then make sure the
following items are checked:

-   Android SDK Platform 29
-   Intel x86 Atom_64 System Image or Google APIs Intel x86 Atom System
    Image

Next, select the \"SDK Tools\" tab and check the box next to \"Show
Package Details\" here as well. Look for and expand the \"Android SDK
Build-Tools\" entry, then make sure that 29.0.2 is selected.

Finally, click \"Apply\" to download and install the Android SDK and
related build tools.

if missing .zshrc file or .bashrc, run touch \~/.bashrc if your terminal
is bash aka(at the title of the terminal window it says bash. If it says
zsh do touch \~/.zshrc), then the export commands for ANDROID_HOME, then
source \~/.\<whichever file you made\>\
\
Run echo \$ANDROID_HOME, this will check to make sure everything is
installed.

Run npm run build:droid-a, this will create the bundle javascript file
for the project

Now follow the following steps

### How to get the simulator running:

### Using a virtual device

If you use Android Studio to open ./AwesomeProject/android, you can see
the list of available Android Virtual Devices (AVDs) by opening the
\"AVD Manager\" from within Android Studio. Look for an icon that looks
like this:

![](screenshots/image3.png)

If you have recently installed Android Studio, you will likely need
to [create a new
AVD](https://developer.android.com/studio/run/managing-avds.html).
Select \"Create Virtual Device\...\", then pick any Phone from the list
and click \"Next\", then select the **Q** API Level 29 image.

If you don\'t have HAXM installed, follow [these
instructions](https://github.com/intel/haxm/wiki/Installation-Instructions-on-macOS) to
set it up, then go back to the AVD Manager.

Click \"Next\" then \"Finish\" to create your AVD. At this point you
should be able to click on the green triangle button next to your AVD to
launch it, then proceed to the next step.

Now you got the emulator spinning up, now you run react-native
run-android.

This is only has to be done once, this is what creates your virtual

This will get the app and running. Whenever you want to make changes you
need to make them in VS Code and click save. The emulator will hot
reload in real time.

If you get a "Unable to find abd" that means you didn't install the OS,
so whatever OS you selected when building your device, make sure that is
checked off in your SDK Manager and installed.

**[BASIC COMMANDS TO KNOW FOR EVERYDAY USE:]{.ul}**

These will be commands and things youll need to do once initially set up
and you need to do development.

1.  open up terminal in the project folder, ie /namp-prototype/

2.  code . or open VS Code and find the namp-prototype folder to open up
    the project

3.  IF YOU ARE WORKING ON ANDROID,

4.  Make whatever changes you want to the code

5.  Save the changes

> This is when things slightly change depending on the platform

6.  Compile/build the app

    a.  ANDROID:
        i.  Open up Android Studio, on the top left youll see a debug
            button, which will look like a play button, select your
            android device in the dropdown, then run it

> ![Graphical user interface, text, application, Teams Description
> automatically
> generated](screenshots/image4.png)

        ii. Npm run dev:droid


b.  iOS:
    i.  simply run npm run dev:ios

c.  The metro bundler window that opens can stay there, you don't have
    to close it if you don't want to when switching between platforms

7.  Make new changes

    a.  iOS: you will need to run npm run dev:ios everytime you want to
        see your changes appear, there is no hot reload capability

    b.  Android: any changes to the react-native (javascript) code, all
        you have to do is save the file and the app will refresh

8.  If you need to make changes to the iOS or Android projects
    themselves you will need to rebuild both meaning run the npm run
    dev:\<platform\>


MORE NOTES ABOUT ANDROID SETUP
Get Android Up and Running:

Open up android studio
Select the app folder, make sure it is selected
Type out nvm run build:droid-a
Then in the device drop down,(next to the debug button) select the proper device
Then click the play button
This will build the application
Then run the react-native run-android

Now you’re environment should be up and running


Now if you make changes to the code the simulator should hot reload and if not then find the metro bundler window and type ‘r’ this will force the app to reload 

\*\*also these commands are not the actual commands, these are aliases
for other commands, to see what the aliases are you can see them in the
package.json file in the scripts {...} section

----

If you install watchman and you at some point get messages saying
"Watchman would like access to your contacts?, like access to
Downloads?" etc, run \`watchman watch-list\`, then see if you see
/Users/\$Users in the root object. If there then run \`watchman
watch-del /Users/\$USER \`Ex) mine looked like this before running the
watch-del command\
\
{

\"version\": \"4.9.0\",

\"roots\": \[

\"/Users/brandonsmac\",\
...\
}\
\
This might also blow up back in your face, so your best bet is to simply
deny or accept the ones you don't want. BUT if you happended to do it
and now are getting watchman errors\
try opendir(/Users/brandonsmac/Library/Application
Support/CloudDocs/session/db) -\> Operation not permitted. Marking this
portion of the tree deleted

To clear this warning, run:

\`watchman watch-del /Users/brandonsmac ; watchman watch-project
/Users/brandonsmac\`

If you end up running into an error with a local server not running like
the following\
run

react-native start \--port=8088\
react-native run-ios \--port 8088

![Graphical user interface, text Description automatically
generated](screenshots/image5.png)

<https://devhints.io/watchman>

Also install expo (which is the step mentioned above)\
\
then the react-native cli\
\
if you get a error saying "no bundle url" follow the steps from
<https://onexlab-io.medium.com/no-bundle-url-present-fixed-ca2688a80f66>
which is mentioned in
<https://stackoverflow.com/questions/65920458/no-bundle-url-found-react-native-getting-started/65921046#65921046>\
\
Then\
Also if you get

**warn** Assets destination folder is not set, skipping\...

then you need to change the build command to react-native bundle
\--entry-file ./index.js \--platform ios \--bundle-output
ios/main.jsbundle \--assets-dest ios

and for android do react-native bundle \--platform android \--dev false
\--entry-file index.js \--bundle-output
android/app/src/main/assets/index.android.bundle \--assets-dest
android/app/src/main/res/

I have gotten it to work for the react-native init and the copy and
paste method I thought of\
let source = require(\'../reactNativePdfSample/ios/assets/test.pdf\');
*// ios only*

I have gotten it to work for the react-native init and the copy and
paste method I thought of, this is the example from the
react-native-pdf. I was able to get it running by pretty much following
the steps on the repo. The best bet is to just follow the steps I did
from the post I made here,
<https://github.com/wonday/react-native-pdf/issues/541>. Which are:

**Repo Steps:**

1.  react-native init \<my-project\>

2.  cd my-project

3.  npm install

4.  npm install react-native-pdf rn-fetch-blob
    \@react-native-community/progress-bar-android
    \@react-native-community/progress-view \--save

5.  cd ios, pod install

6.  cd ..

7.  react-native bundle \--entry-file=\'index.js\'
    \--bundle-output=\'./ios/main.jsbundle\' \--dev=false
    \--platform=\'ios\'

8.  Then I open up the iOS folder in Xcode because I get a no bundle url
    found if I dont do this step, and I go to Xcode Build Phases and add
    the main.jsbundle to the, all explained
    here [[https://onexlab-io.medium.com/no-bundle-url-present-fixed-ca2688a80f66]{.ul}](https://onexlab-io.medium.com/no-bundle-url-present-fixed-ca2688a80f66)

9.  Then run \`react-native run-ios\'

10. THIS IS TO MAKE SURE MY APP IS AT LEAST RUNNING THE \"HELLO WORLD\"
    sample

11. Copy the code from PDFExample.js from the repo,
    i.e [[https://github.com/wonday/react-native-pdf/blob/master/example/PDFExample.js]{.ul}](https://github.com/wonday/react-native-pdf/blob/master/example/PDFExample.js) and
    then adjust the App.js to display this component

12. Also copy over the test.pdf that is at the root level of the
    react-native-pdf

13. Then I run react-native bundle \--entry-file ./index.js \--platform
    ios \--bundle-output ios/main.jsbundle \--assets-dest ios

    a.  This should generate a libraries folder, copy the test.pdf into
        the assets folder that was created

14. Open up the ios folder again in Xcode, and click the project name. I
    then saw a folder called Libraries, right click and click "Add Files
    to Libraries", then find the assets folder and click it and add to
    the project, look something like this, (just ignore the file names
    and stuff)

    a.  ![Graphical user interface, text, application, email Description
        automatically
        generated](screenshots/image6.png)

15. Then run the command from **react-native run-ios** and it should
    spin up the simulator

let source = require(\'../reactNativePdfSample/ios/assets/test.pdf\');
// ios only

If you are trying to debug the console.log("blah blah") I noticed that
can be seen if you run your project in XCode

Also if you begin to get a react-native-snap-carousel not found, simply
go and remove the line from package.json, then run npm install
react-native-snap-carousel

Adding navigation to carousel

<https://github.com/meliorence/react-native-snap-carousel/issues/212>

Functional component version of a react-native-snap-carousel -\>
<https://github.com/meliorence/react-native-snap-carousel/issues/715>\
<https://blog.logrocket.com/using-react-native-to-implement-a-carousel/>

React Native Navigation\
<https://blog.logrocket.com/navigating-react-native-apps-using-react-navigation/#whatisrnn>

To create the carousel react navigation I looked at the documentation in
react-native-snap-carousel and the react-native dribble2react travel-app
to compare and read the code
<https://github.com/meliorence/react-native-snap-carousel/blob/master/doc/TIPS_AND_TRICKS.md#implementing-navigation>
and <https://github.com/react-ui-kit/dribbble2react>

For quizzes I used the react-native-simple-survey. This will allos us to
be able to make questions based on a certain format,
<https://snack.expo.io/TCeU2A4p1>

If you find yourself having issue with Font not found try using
loadFont() command at the top of the file

Also very nifty thing that might help in the long run is BuilderX.io,
this would work wonders if we were to use the Native Base UI Kit as
well.

For search box I used react native elements search box,
<https://reactnativeelements.com/docs/searchbar/#default-searchbar>

And to learn how to customize the header I viewed
<https://stackoverflow.com/questions/44701245/hide-header-in-stack-navigator-react-navigation>

<https://reactnavigation.org/docs/4.x/headers>

<https://nimblewebdeveloper.com/blog/convert-react-class-to-function-component>

To fix ios to be able to run and work on ipad as well as android\
![Graphical user interface Description automatically
generated](screenshots/image7.png)

You need to change the Targeted Device Families from Iphone to iphone
and ipad,
<https://stackoverflow.com/questions/43899020/how-do-i-build-a-react-native-app-so-that-it-runs-as-an-ipad-app-on-an-ipad>

React-Native-Pdf Bookmark Data

*This data comes from the tableOfContents object in the OnLoadComplete
function in the PDF Component*

\[ { title: \'Table of Contents\',

pageIdx: \'4\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Introduction\',

pageIdx: \'5\',

children:

\[ { title: \'What you will learn:\',

pageIdx: \'5\',

children: \[\],

mNativePtr: \'\' },

{ title: \'How to read this book ?\',

pageIdx: \'5\',

children: \[\],

mNativePtr: \'\' },

{ title: \'What will be updated in this book ?\',

pageIdx: \'6\',

children: \[\],

mNativePtr: \'\' } \],

mNativePtr: \'\' },

{ title: \'Installation and setup new project\',

pageIdx: \'7\',

children:

\[ { title: \'Quick start\',

pageIdx: \'7\',

children: \[\],

mNativePtr: \'\' },

{ title: \'A closer look at application creation\',

pageIdx: \'7\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Troubleshooting\',

pageIdx: \'9\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Build and run on device:\',

pageIdx: \'10\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Running On Device\',

pageIdx: \'10\',

children: \[\],

mNativePtr: \'\' },

{ title: \'So what is React-Native ?\',

pageIdx: \'11\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Exercise:\',

pageIdx: \'13\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Solution:\',

pageIdx: \'13\',

children: \[\],

mNativePtr: \'\' } \],

mNativePtr: \'\' },

{ title: \'Appendix: Quick introduction to React.js\',

pageIdx: \'16\',

children:

\[ { title: \'Getting up and running\',

pageIdx: \'16\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Components --- the building blocks of React\',

pageIdx: \'17\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Is that HTML.. in my javascript?\',

pageIdx: \'18\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Components and State\',

pageIdx: \'19\',

children: \[\],

mNativePtr: \'\' } \],

mNativePtr: \'\' } \]

2021-02-02 10:05:17.212 \[info\]\[tid:com.facebook.react.JavaScript\]
current page: 1

2021-02-02 10:05:17.227 \[info\]\[tid:com.facebook.react.JavaScript\]
total page count: 21

2021-02-02 10:05:17.228 \[info\]\[tid:com.facebook.react.JavaScript\] \[
{ title: \'Table of Contents\',

pageIdx: \'4\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Introduction\',

pageIdx: \'5\',

children:

\[ { title: \'What you will learn:\',

pageIdx: \'5\',

children: \[\],

mNativePtr: \'\' },

{ title: \'How to read this book ?\',

pageIdx: \'5\',

children: \[\],

mNativePtr: \'\' },

{ title: \'What will be updated in this book ?\',

pageIdx: \'6\',

children: \[\],

mNativePtr: \'\' } \],

mNativePtr: \'\' },

{ title: \'Installation and setup new project\',

pageIdx: \'7\',

children:

\[ { title: \'Quick start\',

pageIdx: \'7\',

children: \[\],

mNativePtr: \'\' },

{ title: \'A closer look at application creation\',

pageIdx: \'7\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Troubleshooting\',

pageIdx: \'9\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Build and run on device:\',

pageIdx: \'10\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Running On Device\',

pageIdx: \'10\',

children: \[\],

mNativePtr: \'\' },

{ title: \'So what is React-Native ?\',

pageIdx: \'11\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Exercise:\',

pageIdx: \'13\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Solution:\',

pageIdx: \'13\',

children: \[\],

mNativePtr: \'\' } \],

mNativePtr: \'\' },

{ title: \'Appendix: Quick introduction to React.js\',

pageIdx: \'16\',

children:

\[ { title: \'Getting up and running\',

pageIdx: \'16\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Components --- the building blocks of React\',

pageIdx: \'17\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Is that HTML.. in my javascript?\',

pageIdx: \'18\',

children: \[\],

mNativePtr: \'\' },

{ title: \'Components and State\',

pageIdx: \'19\',

children: \[\],

mNativePtr: \'\' } \],

mNativePtr: \'\' } \]

<https://codesandbox.io/s/quirky-shirley-t4wd3?file=/src/index.js>

Code Snippet of how to map the data needed

Goal: Bottom sheet for now to display and have at least a list of the
bookmarks

Bottom Sheet Component
<https://gorhom.github.io/react-native-bottom-sheet/>

<https://reactnativeelements.com/docs/bottomsheet/>

Using the Gorhom:

1.  Follow the instructions from

2.  npm install \'\@gorhom/bottom-sheet@\^2\'

3.  npm install react-native-reanimated react-native-gesture-handler

4.  

Migrate Class Component to Functional Component:

<https://www.codementor.io/@uokesita/react-native-pdf-digital-signature-13ephkmvki>

When you get typescript errors in your js files there is something wrong
with the vscode extensions,
<https://stackoverflow.com/questions/48859169/js-types-can-only-be-used-in-a-ts-file-visual-studio-code-using-ts-check>
but follow the instrcutions here
<https://github.com/flowtype/flow-for-vscode#setup>, I had to end up
cmd+shift+p and typing settings.json to open up the workspace setting,
this way the errors would go away.

Managing Static Files in React Native:
<https://willowtreeapps.com/ideas/react-native-tips-and-tricks-2-0-managing-static-assets-with-absolute-paths>

GitLab

I created an account, added my ssh key 'cat \~/.ssh/id_rsa.pub' to the
ssh keys section of my account then clicked save. If you do not already
have a pub/private key pair, then follow the docs at
<https://docs.gitlab.com/ee/ssh/#rsa-ssh-keys> and
<https://docs.gitlab.com/ee/ssh/>. Then I went into the codebase and
setup repo by following the Push an Existing Folder commands from the
below documentation:

### Command line instructions

You can also upload existing files from your computer using the
instructions below.

##### Git global setup

git config \--global user.name \"Brandon Cratty\"

git config \--global user.email \"bjcratty\@gmail.com\"

##### 

##### Create a new repository

git clone git\@gitlab.com:bjcratty/namp-prototype.git

cd namp-prototype

touch README.md

git add README.md

git commit -m \"add README\"

git push -u origin master

##### **Push an existing folder (commands I used)**

cd existing_folder

git init

git remote add origin git\@gitlab.com:bjcratty/namp-prototype.git

git add .

git commit -m \"Initial commit\"

git push -u origin master

##### Push an existing Git repository

cd existing_repo

git remote rename origin old-origin

git remote add origin git\@gitlab.com:bjcratty/namp-prototype.git

git push -u origin \--all

git push -u origin \--tags

Once I got down to the git push -u origin master command and it said
authenticity cant be established, I clicked yes. This then said could
not read from remote repo. The error is below

##### 

brandonsmac\@Brandons-MacBook-Pro example % git push -u origin master

The authenticity of host \'gitlab.com (172.65.251.78)\' can\'t be
established.

ECDSA key fingerprint is
SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.

Are you sure you want to continue connecting (yes/no/\[fingerprint\])?
yes

Warning: Permanently added \'gitlab.com,172.65.251.78\' (ECDSA) to the
list of known hosts.

Connection closed by 172.65.251.78 port 22

fatal: Could not read from remote repository.

Please make sure you have the correct access rights

and the repository exists.

To get around this, either the commands I ran fixed it or it could have
been the fact that I simply gave it time to catch up with my ssh key
being added. These commands came from me reading this article
<https://forum.gitlab.com/t/gitlab-ssh-key-not-working/36089/4>. Either
way the commands I ran were \`ssh <git@gitlab.com%60> which gave the
outout

PTY allocation request failed on channel 0

Welcome to GitLab, \@bjcratty!

Connection to gitlab.com closed.

Then I ran ssh -vvvT git\@gitlab.com which gave me a long debug output
but near the end it looked like

debug3: channel 0: status: The following connections are open:

\#0 client-session (t4 r0 i3/0 o3/0 e\[write\]/0 fd -1/-1/8 sock -1 cc
-1)

debug3: send packet: type 1

debug3: fd 1 is not O_NONBLOCK

Transferred: sent 3496, received 3048 bytes, in 0.1 seconds

Bytes per second: sent 25650.2, received 22363.3

debug1: Exit status 0

which I infer the connection worked, next when I ran the git push -u
origin master it worked fine.

When trying to push to the repo, it will ask for your username and
password, this should only needed to be asked once because it will be
added to your Keychain as a saved login info

If you would like to pull in changes from master while working on
another branch, use git pull origin master

Standing Up App from Repo:

1.  Git clone <https://gitlab.com/bjcratty/namp-prototype>

2.  Cd namp-prototype

3.  Npm install

4.  Cd ios

5.  Pod install

6.  Cd .. (make sure you are at the project root)

7.  npm run build:ios-a && react-native run-ios

    a.  Npm run build:ios-a

        i.  (this in an alias for a react-native bundle command, feel
            free to change it to something more fitting in the
            package.json file

        ii. These commands were covered earlier but long story short is
            the ios-a command compiles and builds the assets that will
            be needed for the application

    b.  React-native run-ios

        i.  Runs the ios version of the app

Posts I have Made for Answers:

<https://github.com/wonday/react-native-pdf/issues/542>

https://github.com/wonday/react-native-pdf/issues/541

### [WebSocket Exception Error, Unable to connect to host in React Native](https://stackoverflow.com/questions/65942192/websocket-exception-error-unable-to-connect-to-host-in-react-native)

### [Static PDF in React Native using React-Native-Pdf](https://stackoverflow.com/questions/65940902/static-pdf-in-react-native-using-react-native-pdf)

### [No Bundle URL Found - React Native Getting Started](https://stackoverflow.com/questions/65920458/no-bundle-url-found-react-native-getting-started)

### [Services can\'t start due to "No Route to Host"](https://stackoverflow.com/questions/65206503/services-cant-start-due-to-no-route-to-host)

ANDROID

*All these steps are mostly from the internet*

<https://stackoverflow.com/questions/60266497/react-native-failed-to-launch-emulator>

<https://stackoverflow.com/questions/38577669/run-react-native-on-android-emulator>

<https://reactnative.dev/docs/environment-setup>

**react-native run-android errors**

If you have no emulators running ie run abd devices and if you see none
listed then follow the steps below.

Go to the link and open up the Android Studio, click the device icon,
then go to project terminal and run abd devices to make sure an emulator
is running

**\> Task :app:transformNativeLibsWithMergeJniLibsForDebug** FAILED

This means you need to cd android, then ./gradlew clean

Now if you get something like

FAILURE: Build failed with an exception.

\* What went wrong:

Execution failed for task
\':app:transformNativeLibsWithMergeJniLibsForDebug\'.

\> More than one file was found with OS independent path
\'lib/x86/libc++\_shared.so\'

Then more than likely is something with the build.gradle settings in the
android folder, just add

android {

*// compileSdkVersion \_compileSdkVersion*

*// buildToolsVersion \_buildToolsVersion*

*// defaultConfig {*

*// minSdkVersion \_minSdkVersion*

*// targetSdkVersion \_targetSdkVersion*

*// }*

lintOptions {

abortOnError true

}

packagingOptions {

pickFirst \'lib/x86/libc++\_shared.so\'

pickFirst \'lib/x86_64/libjsc.so\'

pickFirst \'lib/x86_64/libc++\_shared.so\'

pickFirst \'lib/arm64-v8a/libjsc.so\'

pickFirst \'lib/arm64-v8a/libc++\_shared.so\'

pickFirst \'lib/armeabi-v7a/libc++\_shared.so\'

}

}

As its own block into the build.gradle. There are 2 build.gradle make
sure you add it to the ./project_root/android/app/build.gradle. If you
don't you will see an error like the following\
\
FAILURE: Build failed with an exception.

\* Where:

Build file
\'/Users/brandonsmac/SysDevMobileApps/FrameworkPrototypes/namp-prototype/android/build.gradle\'
line: 40

\* What went wrong:

A problem occurred evaluating root project \'example\'.

\> Could not find method packagingOptions() for arguments
\[build_es9skneb8qwiyp7b3lyj1vwtx\$\_run_closure2\@423ff183\] on root
project \'example\' of type org.gradle.api.Project.

Once you are able to successfully build and run the react-native
run-android you might come across something like the following.\
![Text Description automatically
generated](screenshots/image8.png){width="2.2558202099737534in"
height="5.031914916885389in"}

Possible Solutions:

<https://stackoverflow.com/questions/54866314/react-native-error-unable-to-resolve-module-index>

The solution I saw that worked was creating an index.android.js file
this is because the build command is pointing to this file.

<https://github.com/wonday/react-native-pdf/issues/21>

<https://stackoverflow.com/questions/55805955/react-native-pdf-showing-on-android-simulator-but-getting-error-on-actual-andro>

For example :

If you have the pdf file at
\"android/app/src/main/assets/pdf/userGuide.pdf\",\
you should use let source = {uri:\'bundle-assets://pdf/userGuide.pdf\'}

In regards to reading local static pdf, need to do a couple things

<https://stackoverflow.com/questions/44446523/unable-to-load-script-from-assets-index-android-bundle-on-windows>

<https://stackoverflow.com/a/35925218/6636638>

Setting up Env

1.  Clone the repo

2.  Run \`npm run build:droid-a && react-native run-android\`

3.  You might see an error like

> \* daemon not running; starting now at tcp:5037
>
> \* daemon started successfully
>
> **info** Launching emulator\...
>
> **error** Failed to launch emulator. Reason: Emulator exited before
> boot..
>
> **warn** Please launch an emulator manually or connect a device.
> Otherwise app may fail to launch.
>
> **info** Installing the app\...
>
> Starting a Gradle Daemon (subsequent builds will be faster)

This just means that you will need to open up Android Studio, open up
the emulator and have it running before running the command from step 2
again. If still errors refer to other notes in the android setup

Possible Articles that might help:
<https://stackoverflow.com/questions/34175416/how-to-use-offline-bundle-on-android-for-react-native-project>
